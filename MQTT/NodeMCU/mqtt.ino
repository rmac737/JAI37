#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// Vars
const char* SSID = "Laboratorio"; // rede wifi
const char* PASSWORD = "senhaForte"; // senha da rede wifi

const char* BROKER_MQTT = "broker"; // ip/host do broker
int BROKER_PORT = 16340; // porta do broker
const char* BROKER_USER = "user";
const char* BROKER_PWD = "password";


#define led_vermelho 13

WiFiClient espClient;
PubSubClient MQTT(espClient); // instancia o mqtt
void configuraWifi(const char *ssid, const char *password);
void configuraLED();
void setup() {
  //serial begin
  Serial.begin(115200);  
  configuraWifi(SSID,PASSWORD);
  configuraLED();

  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  String message;
  for (int i = 0; i < length; i++) 
    message += (char)payload[i];
  
  Serial.println("Tópico => " + String(topic) + " | Valor => " + String(message));
  if (message == "vermelhoon") 
    digitalWrite(led_vermelho, 1);    
  else if (message == "vermelhooff")
    digitalWrite(led_vermelho, 0);      
}

float t =0;
char mensagem[200];
void loop() {  
  verificaWifi();
  if (!MQTT.connected())    
    reconnectMQTT();
  MQTT.loop();
}

void reconnectMQTT() {
  while (!MQTT.connected()) {
    if (MQTT.connect("ESP8266-ESP12-E",BROKER_USER,BROKER_PWD)) {
      Serial.println("Conectado");
      MQTT.subscribe("arduino_lcc_00/SWITCH");
    } else {
      Serial.println("Falha ao Reconectar");
      delay(500);
    }
  }
}

void configuraLED(){
  pinMode(led_vermelho, OUTPUT);
}

void verificaWifi(){
  while (WiFi.status() != WL_CONNECTED)  {
     
    //Wait for connection
    delay(500);
    Serial.println("Esperando conexão...");
  }
}
void configuraWifi(const char *ssid, const char *password){ 
  WiFi.begin(ssid, password); //Connect to the WiFi network
  verificaWifi();
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP()); //Print the local IP
}