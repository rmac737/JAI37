#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <Hash.h>
#include <ArduinoJson.h>

#define IP "10.0.0.10"
#define PORTA 8080
#define SSID "Laboratorio"
#define PASSWORD "senhaforte"

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;
int interval = 3000;

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length)
{
	switch (type)
	{
	case WStype_CONNECTED:
		Serial.println("Conectado");
		webSocket.sendTXT(thisDeviceInfo().c_str());
		break;
	case WStype_TEXT:
		Serial.println("Recebido novos parâmetros do servidor");
		String aux = toString(payload, length);
		StaticJsonBuffer<50> jsonBuffer;
		JsonObject &msg = jsonBuffer.parseObject(aux);
		if (msg.success())
		{
			interval = msg["interval"];
		}
		break;
	}
}
void setup()
{
	Serial.begin(115200);
	WiFiMulti.addAP(SSID, PASSWORD);
	verifyWiFi();
	webSocket.begin(IP, PORTA, "/");
	webSocket.onEvent(webSocketEvent);
	webSocket.setReconnectInterval(5000);
}

void loop()
{
	webSocket.loop();
	webSocket.sendTXT(getReport().c_str());
	delay(interval);
}

String thisDeviceInfo()
{
	return "lorem ipsum";
}
String getReport()
{
	String a = "info ";
	a += interval;
	return a;
}
void verifyWiFi()
{
	while (WiFiMulti.run() != WL_CONNECTED)
	{
		delay(100);
	}
}
String toString(uint8_t *payload, int length)
{
	String message;
	for (int i = 0; i < length; i++)
		message += (char)payload[i];
	return message;
}