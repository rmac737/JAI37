
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HTTPClient {
  public static void main(String[] args) throws Exception {
    HTTPClient http = new HTTPClient();
    http.sendGet("http://dominio/servicos/LEDServer/vermelho");
    http.sendPut("http://dominio/servicos/LEDServer/vermelho");
  }

  // HTTP GET request
  private void sendGet(String url) throws Exception {
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    //con.setRequestMethod("GET");
    int responseCode = con.getResponseCode();
    System.out.println("Código: "+responseCode);
    printInputStream(con.getInputStream());
    con.disconnect();
    
  }

  // HTTP PUT request
  private void sendPut(String url) throws Exception {
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    con.setRequestMethod("PUT");
    con.setRequestProperty("led","0");
    int responseCode = con.getResponseCode();
    System.out.println("Código: "+responseCode);
    printInputStream(con.getInputStream());
    con.disconnect();
  }

  private void  printInputStream(InputStream is) throws Exception{
    BufferedReader in = new BufferedReader(new InputStreamReader(is));
    String inputLine;
    StringBuffer response = new StringBuffer();
    while ((inputLine = in.readLine()) != null)
      response.append(inputLine+"\n");
    System.out.println(response.toString());
    in.close();
  }
  
}