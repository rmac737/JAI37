#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define led_vermelho 13
ESP8266WebServer server(80);

const char *ssid = "Laboratorio";
const char *password = "senhaForte";
const char *headers[] = {"led"};

void configuraLED(){
  pinMode(led_vermelho, OUTPUT);
}

void setup(){

  Serial.begin(115200);
  configuraWifi(ssid, password);
  configuraLED();

  server.on("/servicos/LEDServer/vermelho", HTTP_PUT, handlerPut);
  server.on("/servicos/LEDServer/vermelho", HTTP_GET, handlerGet);

  server.collectHeaders(headers, 1);
  server.begin(); //Inicia o servidor
  Serial.println("Servidor escutando...");
}

void loop(){
  server.handleClient();
}

void handlerPut(){
  Serial.println("Recebi um put");
  if (!server.hasHeader("led"))  {
    server.send(400, "text/plain", "Deve conter um valor no header led!");
    return;
  }
  String valor = String(server.header("led"));
  if (valor == "0")  {
    digitalWrite(led_vermelho, 0);
    server.send(200, "text/plain", "Sucesso, Led desligado");
  }
  else if (valor == "1")  {
    digitalWrite(led_vermelho, 1);
    server.send(200, "text/plain", "Sucesso, Led ligado");
  }
  else  {
    server.send(400, "text/plain", "Valor inválido");
  }
}
void handlerGet(){
  server.send(200, "text/plain", "Lista de recursos\nLed Vermelho");
  delay(1000);
}
void configuraWifi(const char *ssid, const char *password){
  WiFi.begin(ssid, password); //Connect to the WiFi network
  while (WiFi.status() != WL_CONNECTED)  {
    //Wait for connection
    delay(500);
    Serial.println("Esperando conexão...");
  }

  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP()); //Print the local IP
}
