#include <ESP8266WiFi.h>
#include <coap_server.h>


// CoAP server endpoint url callback
void callback_ligaled(coapPacket &packet, IPAddress ip, int port, int obs);

coapServer coap;

//WiFi connection info
const char* ssid = "Laboratorio";
const char* password = "senhaForte";

#define led_verde 12
#define led_vermelho 13

// CoAP server endpoint URL
void callback_ligaled(coapPacket *packet, IPAddress ip, int port,int obs) {
    if(ip.toString().equals("0.0.0.0"))return;
    Serial.println("LigaLed chamado");
    Serial.println(ip);
    Serial.println(port);
    
    char p[packet->payloadlen + 1];
    memcpy(p, packet->payload, packet->payloadlen);
    p[packet->payloadlen] = NULL;
    Serial.println(p);
  
    String message(p);
  
    if (message.equals("0"))
    {
      digitalWrite(led_verde,LOW);
      Serial.println("Desliga LED");
      coap.sendResponse(ip,port,"LED desligado");
    }
    else if (message.equals("1"))
    {
      digitalWrite(led_verde,HIGH);
      Serial.println("Liga LED");
      coap.sendResponse(ip,port,"LED Ligado");
    } else{
        //send response
      coap.sendResponse(ip,port,"Mensagem recebida com sucesso.");
    }
}

void setup() {
  //serial begin
  Serial.begin(115200);  
  configuraWifi(ssid,password);
  configuraLED();

  coap.server(callback_ligaled, "ledVermelho");
  coap.start();
}

void loop() {
  coap.loop();
  delay(1000);
}




void configuraLED(){
  pinMode(led_vermelho, OUTPUT);
}

void configuraWifi(const char *ssid, const char *password){
  WiFi.begin(ssid, password); //Connect to the WiFi network
  while (WiFi.status() != WL_CONNECTED)  {
    //Wait for connection
    delay(500);
    Serial.println("Esperando conexão...");
  }

  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP()); //Print the local IP
}